Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: ramaze
Source: http://ramaze.net

Files: *
Copyright: 2006-2012 Michael Fellinger <m.fellinger@gmail.com>
           Aman Gupta <aman@ramaze.net>
           Jonathan Buch <jonathan.buch@gmail.com>
           Pistos <gitsomegrace.5.pistos@geoshell.com>
           Clive Crous <clive@crous.co.za>
           Keita Yamaguchi <keita.yamaguchi@gmail.com>
           Ryan Grove <ryan@wonko.com>
           injekt <injekt.me@gmail.com>
           TJ Vanderpoel <bougy.man@gmail.com>
           Wang, Jinjing <nfjinjing@gmail.com>
           Mario Gutierrez <mgutz@mgutz-mbp.local>
           Tadahiko Uehara <kikofx@gmail.com>
           Aki Reijonen <aki.reijonen@gmail.com>
           Antti Tuomi <antti.tuomi@tkk.fi>
           Sam Carr <samcarr@gmail.com>
           Dmitry Gorbik <socket.h@gmail.com>
           mwlang <mwlang@cybrains.net>
           Andreas Karlsson <andreas@proxel.se>
           James Tucker <jftucker@gmail.com>
           mig <mig@mypeplum.com>
           Chris Duncan <celldee@gmail.com>
           Clinton R. Nixon <clinton.nixon@viget.com>
           Masahiro Nakagawa <repeatedly@gmail.com>
           Riku Raisaenen <riku@helloit.fi>
           starapor <me@sarahtaraporewalla.com>
           Andrew Farmer <xichekolas@gmail.com>
           jShaf <joshua.shaffner@gmail.com>
           Lars Olsson <lasso@lassoweb.se>
           Yutaka HARA <yutaka.hara+github@gmail.com>
           Aaron Mueller <mail@aaron-mueller.de>
           Ara T. Howard <ara.t.howard@gmail.com>
           Carlo Zottmann <carlo@zottmann.org>
           Cheah Chu Yeow <chuyeow@gmail.com>
           Christian Neukirchen <chneukirchen@gmail.com>
           Colin Shea <colin@centuar.(none)>
           crab <crabtw@gmail.com>
           Dmitry Gorbik <dmitrygorbik@isengard.local>
           Fabian Buch <fabian.buch@fabian-buch.de>
           Gavin Kistner <gavin@phrogz.net>
           Jean-Francois Chevrette <jfchevrette@iweb.ca>
           Jeremy Evans <code@jeremyevans.net>
           kez <kdobson@gmail.com>
           Martin Hilbig <blueonyx@dev-area.net>
           Matt Rubens <mrubens@goldencookie.localdomain>
           Rob Lievaart <rob@rebeltechnologies.nl>
           sean <sean@sean-t61p.(none)>
           Thomas Leitner <thomas.leitner@gmail.com>
           Victor Luft <victor.luft@ptomato.net>
           Vincent Roy <VincentRoy8@gmail.com>
           Yasushi Abe <yasushi.abe@gmail.com>
           Zoxc <zoxc32@gmail.com>
License: MIT

Files: debian/*
Copyright: 2008 Sebastien Delafond <seb@debian.org>
	   2010 Deepak Tripathi <apenguinlinux@gmail.com>
	   2012 Cédric Boutillier <cedric.boutillier@gmail.com>
	   2013 Gunnar Wolf <gwolf@gwolf.org
License: GPL-2

Files: lib/proto/public/js/jquery.js
Copyright: 2008 John Resig
License: MIT or GPL-2

Files: lib/ramaze/snippets/ramaze/lru_hash.rb
Copyright: 2002 Yoshinori K. Okuji <okuji@enbug.org>
  	   2009  Michael Fellinger  <manveru@rubyists.com>
License: Ruby

Files: lib/ramaze/log/syslog.rb spec/ramaze/log/syslog.rb
Copyright: 2008 <rob@rebeltechnologies.nl>
	   2008 Michael Fellinger <m.fellinger@gmail.com>
License: MIT

License: MIT
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.

License: GPL-2
 This program is free software; you can redistribute it
 and/or modify it under the terms of the GNU General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later
 version.
 .
 This program is distributed in the hope that it will be
 useful, but WITHOUT ANY WARRANTY; without even the implied
 warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 PURPOSE.  See the GNU General Public License for more
 details.
 .
 You should have received a copy of the GNU General Public
 License along with this package; if not, write to the Free
 Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 Boston, MA  02110-1301 USA
 .
 On Debian systems, the full text of the GNU General Public
 License version 2 can be found in the file
 `/usr/share/common-licenses/GPL-2'.

License: Ruby
 You can redistribute it and/or modify it under either the terms of the
 2-clause BSDL (see the file BSDL), or the conditions below:
 .
  1. You may make and give away verbatim copies of the source form of the
     software without restriction, provided that you duplicate all of the
     original copyright notices and associated disclaimers.
 .
  2. You may modify your copy of the software in any way, provided that
     you do at least ONE of the following:
 .
       a) place your modifications in the Public Domain or otherwise
          make them Freely Available, such as by posting said
          modifications to Usenet or an equivalent medium, or by allowing
          the author to include your modifications in the software.
 .
       b) use the modified software only within your corporation or
          organization.
 .
       c) give non-standard binaries non-standard names, with
          instructions on where to get the original software distribution.
 .
       d) make other distribution arrangements with the author.
 .
  3. You may distribute the software in object code or binary form,
     provided that you do at least ONE of the following:
 .
       a) distribute the binaries and library files of the software,
          together with instructions (in the manual page or equivalent)
          on where to get the original distribution.
 .
       b) accompany the distribution with the machine-readable source of
          the software.
 .
       c) give non-standard binaries non-standard names, with
          instructions on where to get the original software distribution.
 .
       d) make other distribution arrangements with the author.
 .
  4. You may modify and include the part of the software into any other
     software (possibly commercial).  But some files in the distribution
     are not written by the author, so that they are not under these terms.
 .
     For the list of those files and their copying conditions, see the
     file LEGAL.
 .
  5. The scripts and library files supplied as input to or produced as
     output from the software do not automatically fall under the
     copyright of the software, but belong to whomever generated them,
     and may be sold commercially, and may be aggregated with this
     software.
 .
  6. THIS SOFTWARE IS PROVIDED "AS IS" AND WITHOUT ANY EXPRESS OR
     IMPLIED WARRANTIES, INCLUDING, WITHOUT LIMITATION, THE IMPLIED
     WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
     PURPOSE.
 .
 Content of the BSDL file:
 .
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 1. Redistributions of source code must retain the above copyright
 notice, this list of conditions and the following disclaimer.
 2. Redistributions in binary form must reproduce the above copyright
 notice, this list of conditions and the following disclaimer in the
 documentation and/or other materials provided with the distribution.
 .
 THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 SUCH DAMAGE.
