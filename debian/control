Source: ruby-ramaze
Section: ruby
Priority: optional
Maintainer: Debian Ruby Extras Maintainers <pkg-ruby-extras-maintainers@lists.alioth.debian.org>
Uploaders: Cédric Boutillier <boutil@debian.org>,
           Gunnar Wolf <gwolf@debian.org>
Build-Depends: debhelper (>= 9~),
               gem2deb,
               ruby-innate
Standards-Version: 3.9.7
Vcs-Git: https://anonscm.debian.org/git/pkg-ruby-extras/ruby-ramaze.git
Vcs-Browser: https://anonscm.debian.org/cgit/pkg-ruby-extras/ruby-ramaze.git
Homepage: http://ramaze.net/
XS-Ruby-Versions: all

Package: ruby-ramaze
Architecture: all
XB-Ruby-Versions: ${ruby:Versions}
Depends: ruby | ruby-interpreter,
         ruby-erubis,
         ruby-haml,
         ruby-hpricot,
         ruby-innate,
         ruby-liquid,
         ruby-locale,
         ruby-maruku,
         ruby-mustache,
         ruby-rack,
         ruby-rdiscount,
         ruby-sass,
         ruby-sequel,
         ruby-slim,
         ${misc:Depends},
         ${shlibs:Depends}
Replaces: libramaze-ruby (<< 2012.12.08-1~),
          libramaze-ruby1.8 (<< 2012.12.08-1~),
          libramaze-ruby1.9.1 (<< 2012.12.08-1~)
Breaks: libramaze-ruby (<< 2012.12.08-1~),
        libramaze-ruby1.8 (<< 2012.12.08-1~),
        libramaze-ruby1.9.1 (<< 2012.12.08-1~)
Provides: libramaze-ruby,
          libramaze-ruby1.8,
          libramaze-ruby1.9.1
Description: Simple and modular web framework
 Ramaze is a very simple and straight-forward web-framework.
 The philosophy of it could be expressed in a mix of KISS and POLS,
 trying to make simple things simple and complex things possible and fun.

Package: libramaze-ruby
Section: oldlibs
Priority: extra
Architecture: all
Depends: ruby-ramaze,
         ${misc:Depends}
Description: Transitional package for ruby-ramaze
 This is a transitional package to ease upgrades to the ruby-ramaze
 package. It can safely be removed.

Package: libramaze-ruby1.8
Section: oldlibs
Priority: extra
Architecture: all
Depends: ruby-ramaze,
         ${misc:Depends}
Description: Transitional package for ruby-ramaze
 This is a transitional package to ease upgrades to the ruby-ramaze
 package. It can safely be removed.

Package: libramaze-ruby1.9.1
Section: oldlibs
Priority: extra
Architecture: all
Depends: ruby-ramaze,
         ${misc:Depends}
Description: Transitional package for ruby-ramaze
 This is a transitional package to ease upgrades to the ruby-ramaze
 package. It can safely be removed.
