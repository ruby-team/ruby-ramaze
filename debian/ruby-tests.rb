# TERRIBLY MINIMAL TEST!
#
# We are currently NOT running any real tests on package build!
#
# To run test, we have to build-depend on the following packages:
#
# ruby-bacon, ruby-slim, ruby-liquid, ruby-sequel
#
# And to package (at least) the following gems, not yet available:
#
# rack/contrib erector remarkably tenjin nagoro lokar ezamar slippers
# tagz redis localmemcache dalli
#
# The proper way to run the test suite would be with 'rake bacon'
# AFACIT.
#
# The only test I can presently do is to check if the library loads
# successfully.

$: << 'lib'
require 'ramaze'
